#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "ir.h"

/* Driver for IR sensors, assumes the following pinouts 
 * Left = PD0 (used to be PC4)
 * Middle = PD1 (used to be PC5)
 * Right = PD2 (used to be PC7)
 */

void ir_init() {
    // enable falling edge interrupt
    PORTD.PIN0CTRL = PORT_ISC_FALLING_gc;
    PORTD.PIN1CTRL = PORT_ISC_FALLING_gc;
    PORTD.PIN2CTRL = PORT_ISC_FALLING_gc;

	//set all in INT0
	PORTD.INT0MASK |= (1 << PIN0) | (1 << PIN1) | (1 << PIN2);
	PORTD.INTCTRL |= PORT_INT0LVL_HI_gc;
}

void ir_charge(unsigned char pinmask) {
    // Recharge the cap(s)
    PORTD.OUT |= pinmask;
    PORTD.DIR |= pinmask;
    _delay_us(200);
    // Set as (floating) input
    PORTD.DIR &= ~pinmask;
}

void ir_start_measurement(IRSensor *ir, uint32_t time) {
    ir->read = 0;
    ir->chargeTime = time;
}

void ir_end_measurement(IRSensor *ir, uint32_t time) {
    // Sensor has already been read. This is needed because
    // each INT handler may have multiple IR sensors and it
    // can't tell which one triggered the interrupt
    if (ir->read)
        return;
    uint32_t diff = time - ir->chargeTime;
    if (diff > UINT16_MAX)
        diff = UINT16_MAX;
    ir->value = diff;
    ir->on = (ir->value > ir->blackThreshold);
    ir->read = 1;
}

void ir_begin_calibrate(IRSensor *ir) {
    ir->minValue = UINT16_MAX;
    ir->maxValue = 0;
}

void ir_calibrate(IRSensor *ir) {
    if (ir->value > ir->maxValue)
        ir->maxValue = ir->value;
    if (ir->value < ir->minValue)
        ir->minValue = ir->value;
}

void ir_end_calibrate(IRSensor *ir) {
    // Take the midpoint of max and min values
    ir->blackThreshold = ir->minValue + (ir->maxValue - ir->minValue) / 2;
}
