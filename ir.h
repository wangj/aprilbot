#ifndef _ir_h
#define _ir_h

#include <stdint.h>

typedef struct {
    // Time interval of discharge (longer time = less reflective)
    uint16_t value;
    // Time interval for black line (tuning parameter)
    uint16_t blackThreshold;
    // Time the sensor was last charged
    uint32_t chargeTime;
    // Boolean value indicating "on black line"
    uint8_t on;
    // Boolean value indicating sensor has been read
    uint8_t read;

    // For calibration
    uint16_t minValue;
    uint16_t maxValue;
} IRSensor;

void ir_init();
void ir_charge(unsigned char pinmask);
void ir_start_measurement(IRSensor *ir, uint32_t time);
void ir_end_measurement(IRSensor *ir, uint32_t time);
void ir_begin_calibrate(IRSensor *ir);
void ir_calibrate(IRSensor *ir);
void ir_end_calibrate(IRSensor *ir);

#endif