#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include <stdint.h>

#define NUM_ADC 7
#define NUM_SAMPLES 16

volatile uint16_t adcValue[NUM_ADC] = {0};

// ISR assumes pins PA1:4
ISR(ADCA_CH0_vect) {
    static uint16_t adcSum[NUM_ADC];
    static uint8_t i = 0;
    static uint8_t nSamples = 0;

    adcSum[i] += ADCA.CH0.RES;
    nSamples += 1;

    // take NUM_SAMPLES readings and average them
    if (nSamples == NUM_SAMPLES) {
        nSamples = 0;
        adcValue[i] = adcSum[i] / NUM_SAMPLES;
        adcSum[i] = 0;

        // move to the next ADC pin
        i = (i+1) % NUM_ADC;
        // index 0 = pin PA1, etc.
        ADCA.CH0.MUXCTRL = (i+1) << 3;
    }
    // keep converting
    ADCA.CH0.CTRL |= ADC_CH_START_bm;
}


void pot_init() {
    // configure A0-A7 as input
    PORTA.DIR = 0xFF;
    // enable adc
    ADCA.CTRLA |= 0x1;
    // 12-bit unsigned
    ADCA.CTRLB = ADC_RESOLUTION_12BIT_gc;
    // use AREF (PA0) reference
    //ADCA.REFCTRL = ADC_REFSEL_AREFA_gc;
    // use 1V internal ref
    ADCA.REFCTRL = ADC_REFSEL_INT1V_gc;
    // prescaler (29MHz/256=114kHz)
    ADCA.PRESCALER = ADC_PRESCALER_DIV256_gc;
    // setup adc channel 0
    ADCA.CH0.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
    ADCA.CH0.INTCTRL = ADC_CH_INTLVL_LO_gc;

    // start with PA1
    ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc;
    ADCA.CH0.CTRL |= ADC_CH_START_bm;
}

uint16_t pot_read(uint8_t i) {
    uint16_t val;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        val = adcValue[i];
    }
    return val;
}
