/* APRILBot V1.0
 * Code by Justin Tesmer
 * APRIL Lab
 * University of Michigan
 * 1/MAY/2013
 * CHANGES:
 * TO DO:
 */


#define DEBUG 0

//--- Constants

//--- System
#define F_CPU                           29491200UL   //7.3728x4PLL

#define OFF                                     0
#define ON                                      1
#define TOGGLE                          2

//--- General USART
#define buffLength                      256//power of 2 buffer length
#define buffMask                        0xFF//faster than modding the address
#define RXbuffLength            128//power of 2 buffer length
#define RXbuffMask                      0x7F//faster than modding the address

#define BPS_115200                      15

//--- ADC
#define ADCACAL1_offset         0x20
#define ADCACAL2_offset         0x20
#define ADCACAL3_offset         0x20
#define ADCACAL4_offset         0x20

//--- OLED      
#define OLEDUSART                       USARTD1//OLED USART port
#define OLEDUSARTPORT                   PORTD
#define OLEDUSART_RXC_vect              USARTD1_RXC_vect
#define OLEDUSART_TX_PIN                PIN7
#define OLEDUSART_DRE_vect              USARTD1_DRE_vect

#define messageWidth                    12// number of characters in message to print on screen
#define OLED_TIMEOUT                    75//16-bit OLED millisecond timeout


// drive state
enum {
    DRIVE_STOP,
    DRIVE_STRAIGHT,
    DRIVE_LEFT,
    DRIVE_RIGHT
};


//--- Externals
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "ir.h"
#include "pot.h"


//--- Global Vars

//volatile unsigned char followLine;
volatile uint8_t state;
volatile uint8_t drive_state = DRIVE_STRAIGHT;
enum {
    STOP,
    FOLLOW_LINE,
    DEAD_RECKON,
    CALIBRATE
};

//--- IR sensors
IRSensor irLeft = {0},
    irMid = {0},
    irRight = {0};

//--- DIP Switches
uint8_t DIP[4] = {0};
/*
struct {
    unsigned char SW1;  //switch state [1=ON, 0=OFF]
    unsigned char SW2;  //switch state [1=ON, 0=OFF]
    unsigned char SW3;  //switch state [1=ON, 0=OFF]
    unsigned char SW4;  //switch state [1=ON, 0=OFF]
} DIP;
*/

//Mag Switch
struct {
    unsigned char SW;   //switch state [1=ON, 0=OFF]
    unsigned char LASTSW;
    unsigned char COUNT; //number of ON transitions
} MAG = {0};


//--- Motors
#define maxSpeed 50
#define minSpeed 20		//minimum speed for robot to move.  Used in Pot mapping

struct {
    char speed;         //current speed percent [-100 to 100]
} M1;

struct {
    char speed;         //current speed percent [-100 to 100]
} M2;

#if DEBUG
//--- OLED Screen
volatile unsigned char OLEDbuffer[buffLength];
volatile unsigned int OLEDbuffIn = 0;
volatile unsigned int OLEDbuffOut = 0;

volatile unsigned char OLEDRXbuffer[RXbuffLength];
volatile unsigned int OLEDRXbuffIn = 0;
volatile unsigned int OLEDRXbuffOut = 0;

volatile unsigned char newOLEDRX = 0;
volatile unsigned char badOLED = 0;
#endif

// Upper 16 bits of globalTime
volatile unsigned int globalTime = 0;

unsigned int atomicReadInt(unsigned int *i)
{
    unsigned char *p = (unsigned char *)i;
    unsigned char h, l;
    do {
        // Read H, then L
        h = *(p+1);
        l = *p;
        // Read H again; if it changed, continue
    } while (*(p+1) != h);

    return ((unsigned int)h << 8) | l;
}

/* Each unit of time represents (F_CPU/2)^-1 = 69ns */
unsigned long getTime()
{
    unsigned int h, l;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        h = globalTime;
        l = TCD1.CNT;
    }

    return ((unsigned long)h << 16) | l;
}


//--- Function Prototypes
//--- System
void init_clock();
void init_sensor_timer();
void init_io();
        
//--- Motors
void init_motors();
void MOTOR (unsigned char id, signed char speed);

//--- UI
void LED (unsigned char id, unsigned char state);

#if DEBUG
//---OLED
//OLED utils
void init_OLED(); 
void OLEDUSART_INIT(int BSEL);
void OLEDUSART_Trans(unsigned char data);
void OLEDwait();

//OLED outputs
void OLED_string(unsigned char x, unsigned char y, char *str);
void OLED_int(long int n, unsigned char x, unsigned char y);
void OLED_int_trunc(long int n, unsigned char x, unsigned char y);
#endif

//--- Interrupt Service Routines

//IR Sensors ISR
ISR(PORTD_INT0_vect) {
    uint32_t time = getTime();

	//IR Middle
    // Check which sensor triggered the interrupt
    if ((PORTD.IN & (1 << PIN1)) == 0) {
        ir_end_measurement(&irMid, time);
        if (state == CALIBRATE)
            ir_calibrate(&irMid);
    }

	//IR Left/Right
    // Check which sensor triggered the interrupt
    if ((PORTD.IN & (1 << PIN0)) == 0) {
        ir_end_measurement(&irLeft, time);
        if (state == CALIBRATE)
            ir_calibrate(&irLeft);
    }
    if ((PORTD.IN & (1 << PIN2)) == 0) {
        ir_end_measurement(&irRight, time);
        if (state == CALIBRATE)
            ir_calibrate(&irRight);
    }

}






//--- 221.25Hz (4.52 ms) IR update loop
ISR(TCD1_OVF_vect) {
    globalTime += 1;

    // Start a recharge
    ir_charge((1 << PIN0) | (1 << PIN1) | (1 << PIN2));
    uint32_t time = getTime();
    ir_start_measurement(&irLeft, time);
    ir_start_measurement(&irMid, time);
    ir_start_measurement(&irRight, time);
}


//--- Pushbutton ISR
ISR(PORTD_INT1_vect) {
	static uint32_t btnDownTime = 0UL;

	//Pushbutton   
    // Button down
    if ((PORTD.IN & (1 << PIN5)) == 0) {
        btnDownTime = getTime();
    }
    // Button up
    else {
        uint32_t diff = getTime() - btnDownTime;
        // Long press (over 1 second): calibration mode
        if (diff > F_CPU/2) {
            //LED(1, ON);
            //LED(2, OFF);
            state = CALIBRATE;
            ir_begin_calibrate(&irLeft);
            ir_begin_calibrate(&irMid);
            ir_begin_calibrate(&irRight);
        } 
        // Short press from stop: line follow mode
        else if (state == STOP) {
            LED(1, OFF);
            LED(2, ON);
            state = FOLLOW_LINE;
            drive_state = DRIVE_STRAIGHT;
            MAG.COUNT = 0;
        }
        // Short press otherwise: stop mode
        else {
            LED(1, OFF);
            LED(2, OFF);
            if (state == CALIBRATE) {
                ir_end_calibrate(&irLeft);
                ir_end_calibrate(&irMid);
                ir_end_calibrate(&irRight);
            }
            state = STOP;
        }
    }

}


#if DEBUG
//--- OLED
ISR(OLEDUSART_DRE_vect) {
    if (OLEDbuffOut == OLEDbuffIn) {//buffer empty
        OLEDUSART.CTRLA &= ~USART_DREINTLVL_MED_gc;//disable data register empty interrupt
    } else {
        OLEDUSART.DATA = OLEDbuffer[OLEDbuffOut++];
        OLEDbuffOut &= buffMask;
    }
}

ISR(OLEDUSART_RXC_vect) {
    OLEDRXbuffer[OLEDRXbuffIn++] = OLEDUSART.DATA;
    OLEDRXbuffIn &= RXbuffMask;

    newOLEDRX = 1;
}
#endif


/* Linearizes potentiometer val (0 to 4096) to the range [0,1]
 * based on a piecewise linear model. Rather approximate.
 */
float linearize(uint16_t val) {
    if (val < 200)
        return 0.0;
    else if (val < 600)
        return (val-200) / 480.0;
    else if (val < 4096)
        return (val-600) / 20976.0 + (5.0/6.0);
    else
        return 1.0;
}


//--- MAIN

int main(void)
{
#if DEBUG
        unsigned int printCount = 0; 
#endif

        
        // Initialize System Clock
        init_clock();

        //Initialize Timers
        init_sensor_timer();

        // Initialize IO ports
        init_io();

        //Initialize Motor Outputs
        init_motors();

    	pot_init();

        //Initialize QTR Reflectance Sensors
    	ir_init();

#if DEBUG
        //USART Init
        OLEDUSART_INIT(BPS_115200);//Initialize OLED USART to 115,200 bps
#endif

                
        PMIC.CTRL |= PMIC_LOLVLEN_bm;//enable low-level interrupts (button)
        PMIC.CTRL |= PMIC_MEDLVLEN_bm; //enable mid-level interrupts (ADC)
        PMIC.CTRL |= PMIC_HILVLEN_bm; //enable high level interrupts (IR sensors)

        sei();// Enable Global interupts

#if DEBUG
        //configure OLED screen
        init_OLED();

		OLED_string(1, 1, "M1");
        OLED_string(1, 2, "M2");
        OLED_string(1, 3, "L");
        OLED_string(1, 4, "C");
        OLED_string(1, 5, "R");
		//OLED_string(1, 3, "Pot 0");
        //OLED_string(1, 4, "Pot 1");
        //OLED_string(1, 5, "Pot 2");
        //OLED_string(1, 6, "Pot 3");
		//OLED_string(1, 7, "Pot 4");
		//OLED_string(1, 8, "Pot 5");
		OLED_string(1, 8, "Count");
		OLED_string(1, 9, "Pot 6");
#endif



    state = STOP;
        
        
    //main loop
    uint32_t magStart = 0UL;
    uint32_t deadreckonStart = 0UL;
    uint32_t deadreckonInterval = 0UL;
    while(1){
        uint32_t time = getTime();

        //check DIP switch states
        DIP[0] = ((PORTE.IN & (1<<PIN0)) == 0);
        DIP[1] = ((PORTE.IN & (1<<PIN1)) == 0);
        DIP[2] = ((PORTE.IN & (1<<PIN2)) == 0);
        DIP[3] = ((PORTE.IN & (1<<PIN3)) == 0);

        //check Magnetic Switch state on PB3
        // Poll every ~2.3ms for switch debouncing
        if (time - magStart > 32767) {
            magStart = time;

            MAG.LASTSW = MAG.SW;
            MAG.SW = ((PORTB.IN & (1 << PIN3)) == 0);
            if (MAG.SW && (MAG.SW != MAG.LASTSW)) {
                MAG.COUNT += 1;
                if (MAG.COUNT > 4) MAG.COUNT = 1;

                if (state == FOLLOW_LINE) {
                    state = DEAD_RECKON;

                    // Set dead reckon time based on pot 6
                    // 2^26 gives up to (2 / (29 MHz)) * 32000000 = 2s
		    deadreckonStart = time;
		    deadreckonInterval = linearize(pot_read(6)) * 32000000UL;

                    // Set direction based on DIP switch
                    if (DIP[MAG.COUNT-1]) {
                        // drive left
                        MOTOR(1, 30);
                        MOTOR(2, 0);
                    } else {
                        // drive right
                        MOTOR(1, 0);
                        MOTOR(2, 30);
                    }
                }
            }
        }

        //indicate Mag Switch
        //LED(1, MAG.SW);

        static uint8_t blinkState = 0;
        if (MAG.SW) {
            LED(1, ON);
            blinkState = 0xFF; // start from long pause
        } else {
            // blink out the next dead reckoning setting number
            static uint32_t blinkStart = 0UL;
	    static uint32_t blinkInterval = 0UL;

            if (time - blinkStart > blinkInterval) {
                if (blinkState >= (MAG.COUNT % 4) + 1) {
                    // long pause
                    blinkState = 0;
                    LED(1, OFF);
		    blinkStart = time;
		    blinkInterval = 16000000UL;
                } else if (PORTC.OUT & (1 << PIN6)) {
                    // if LED on
                    LED(1, OFF);
		    blinkStart = time;
		    blinkInterval = 2000000UL;
                } else {
                    // if LED off
                    LED(1, ON);
                    blinkState += 1;
		    blinkStart = time;
		    blinkInterval = 1500000UL;
                }
            }
        }
        //*/

        //set IR indicator LEDs
        // if no leds are on, indicate latched state
        if (state == FOLLOW_LINE &&
            !irRight.on && !irMid.on && !irLeft.on) {
            static uint32_t blinkStart = 0L;

	    // blink 4Hz
            if (time - blinkStart > 2000000UL) {
		blinkStart = time;
                switch(drive_state) {
                case DRIVE_RIGHT:
                    LED(3, TOGGLE);
                    break;
                case DRIVE_STRAIGHT:
                    LED(4, TOGGLE);
                    break;
                case DRIVE_LEFT:
                    LED(5, TOGGLE);
                    break;
                }
            }
        } else {
            LED(3, irRight.on);
            LED(4, irMid.on);
            LED(5, irLeft.on);
        }

        //--- Run Line follow
        if (state == FOLLOW_LINE) {
            uint16_t valL = 0, valR = 0;
            int8_t m1 = 0, m2 = 0;

            if (irLeft.on && !irRight.on) {
                // turn left
                drive_state = DRIVE_LEFT;
            } else if (irRight.on && !irLeft.on) {
                // turn right
                drive_state = DRIVE_RIGHT;
            } else if (irMid.on) {
                // drive straight
                drive_state = DRIVE_STRAIGHT;
            } 

            if (drive_state == DRIVE_STRAIGHT) {
                valR = pot_read(0);
				valL = pot_read(1);
            } else if (drive_state == DRIVE_LEFT) {
                valR = pot_read(4);
				valL = pot_read(5);
            } else if (drive_state == DRIVE_RIGHT) {
                valR = pot_read(2);
				valL = pot_read(3);
            } 

            // Linearize / scale pot reading and apply deadband
            if (valR < 200)
                m1 = 0;
            else
                m1 = (int8_t)((maxSpeed-minSpeed) * linearize(valR)) + minSpeed;
            if (valL < 200)
                m2 = 0;
            else
                m2 = (int8_t)((maxSpeed-minSpeed) * linearize(valL)) + minSpeed;

            MOTOR(1, m1);
            MOTOR(2, m2);
        } else if (state == DEAD_RECKON) {
            static uint32_t blinkStart = 0L;

            if (time - blinkStart > 1000000UL) {
                LED(2, TOGGLE);
                blinkStart = time;
            }

            // After enforced 0.5s straight, keep driving straight
            // until line is encountered
	    uint32_t dtime = time - deadreckonStart;
            if (dtime > deadreckonInterval + 4000000UL) {
                state = FOLLOW_LINE;
                LED(2, ON);
                drive_state = DRIVE_STRAIGHT;
            }
	    // After time is up
	    // drive straight for a bit to avoid seeing same line
	    // (about 0.5 second -- set above)
	    else if (dtime > deadreckonInterval) {
                MOTOR(1, 30);
                MOTOR(2, 30);
            }
        } else {
            MOTOR(1, 0);
            MOTOR(2, 0);
        }

        if (state == CALIBRATE) {
            // flash green LED slowly
            static uint32_t blinkStart = 0L;

            if (time - blinkStart > 6000000UL) {
                LED(2, TOGGLE);
                blinkStart = time;
            }
        }



#if DEBUG
        //--- Print
        if (printCount > 100) { //print every 100 main loops
            printCount = 0;

			OLED_int(M1.speed, 9, 1);
            OLED_int(M2.speed, 9, 2);

            OLED_int(irLeft.value, 9, 3);
            OLED_int(irMid.value, 9, 4);
            OLED_int(irRight.value, 9, 5);

            /*
            OLED_int(100*linearize(pot_read(0)), 9, 3);
            OLED_int(100*linearize(pot_read(1)), 9, 4);
            OLED_int(100*linearize(pot_read(2)), 9, 5);
            OLED_int(100*linearize(pot_read(3)), 9, 6);
			OLED_int(100*linearize(pot_read(4)), 9, 7);
			OLED_int(100*linearize(pot_read(5)), 9, 8);
            */
            OLED_int(MAG.COUNT, 9, 8);
            OLED_int(100*linearize(pot_read(6)), 9, 9);
        }
        else
            printCount++;
#endif               

                

    }

    return 0;


}

void init_clock() {


        //--------------------------------------------------------------------------------
        //
        // Desc: Configures system clock
        //
        // Args:        none
        //
        // Return:      none
        //
        //--------------------------------------------------------------------------------


        OSC.XOSCCTRL=OSC_FRQRANGE_2TO9_gc|OSC_XOSCSEL_XTAL_16KCLK_gc;   //Set X Osc Ctrl for external clock, 2MHz-9MHz
        OSC.CTRL|=OSC_XOSCEN_bm;                                                                                //enable external oscillator
        while(!(OSC.STATUS&OSC_XOSCRDY_bm));                                                    //wait for crystal to stabalize

        OSC.PLLCTRL=OSC_PLLSRC_XOSC_gc|0x04;                                                    //set PLL to external oscillator, 4x factor
        OSC.CTRL|=OSC_PLLEN_bm;
        while(!(OSC.STATUS&OSC_PLLRDY_bm));                                                             //wait for PLL to stabalize

        CCP=CCP_IOREG_gc;                                                                               //Set Configuration Change Register to protect IO signature
        CLK.CTRL=CLK_SCLKSEL_PLL_gc;                                                    //switch system clock to PLL
        OSC.CTRL&=~OSC_RC2MEN_bm;                                                               //disable 2MHz oscillator
}




/*
// Desc: initializes timer 0 as timer for the mag switch special action
//
// Return: none
void init_magsw_timer() {
    //TCD0.PER = 14400;//set top of Timer/Counter D0

    //Set Timer/Counter D0 overflow interrupt to low-level
    TCD0.INTCTRLA |= TC_OVFINTLVL_LO_gc;
    //set Timer/Counter D0 to F_CPU/1024
    // at F_CPU=29MHz this provides a max time of 2.276 seconds
    TCD0.CTRLA = TC_CLKSEL_DIV1024_gc;
}
*/




// Desc: initializes timer 1 as a global timer and uses its overflow
//       interrupt for sensor updates
//
// Return: none
void init_sensor_timer() {
    // Set Timer/Counter D1 overflow interrupt to high-level
    TCD1.INTCTRLA |= TC_OVFINTLVL_HI_gc;
    // Set Timer/Counter D1 to F_CPU/64 (gives roughly 2us resolution)
    TCD1.CTRLA = TC_CLKSEL_DIV2_gc;        
}


void init_io() {


        //--------------------------------------------------------------------------------
        //
        // Desc: initializes IO ports for pushbutton, SYS_EN, DIP and Magnetic Switches
        //
        // Args:        none
        //
        // Return:      none
        //
        //--------------------------------------------------------------------------------


        //init SYS_EN PIN on PD4
        PORTD.PIN4CTRL |= PORT_OPC_PULLUP_gc;
        _delay_us(1); //let pullup rise
        PORTD.DIR |= (1 << PIN4);
        PORTD.OUT |= (1 << PIN4);

        //init LED1 (red) on PC6
        PORTC.DIR |= (1 << PIN6);
        PORTC.OUT &= ~(1 << PIN6);

        //init LED2 (green) on PC7
        PORTC.DIR |= (1 << PIN7);
        PORTC.OUT &= ~(1 << PIN7);

		//init IR indicator LEDs (LED 3, 4 and 5) on PB0, 1 and 3
		PORTB.DIR |= (1 << PIN0) | (1 << PIN1) | (1 << PIN2);
		PORTB.OUT &= ~(1 << PIN0);
		PORTB.OUT &= ~(1 << PIN1);
		PORTB.OUT &= ~(1 << PIN2);

        //init pushbutton on PC1
        //PORTC.PIN1CTRL |= PORT_OPC_PULLUP_gc;
        //_delay_us(1); //let pullup rise
        //PORTC.INT0MASK |= (1 << PIN1); //enable C1 to trigger interrupt C0
        //PORTC.INTCTRL = PORT_INT0LVL_LO_gc;//Set interrupt 1 to low level

		//init pushbutton on PD5
        PORTD.PIN5CTRL |= PORT_OPC_PULLUP_gc;
        _delay_us(1); //let pullup rise
        PORTD.INT1MASK |= (1 << PIN5); //enable D5 to trigger interrupt D1
        PORTD.INTCTRL |= PORT_INT1LVL_LO_gc;//Set interrupt 1 to low level


        //init DIP switches on PE0-3
        PORTE.PIN0CTRL |= PORT_OPC_PULLUP_gc;
        PORTE.PIN1CTRL |= PORT_OPC_PULLUP_gc;
        PORTE.PIN2CTRL |= PORT_OPC_PULLUP_gc;
        PORTE.PIN3CTRL |= PORT_OPC_PULLUP_gc;
        _delay_us(1); //let pullup rise
        //set as input in DDRE
        PORTE.DIR &= ~(1 << PIN0);
        PORTE.DIR &= ~(1 << PIN1);
        PORTE.DIR &= ~(1 << PIN2);
        PORTE.DIR &= ~(1 << PIN3);

        //init Magnetic Switch on PA7
        //PORTA.PIN7CTRL |= PORT_OPC_PULLUP_gc;
        //_delay_us(1);
        //PORTA.DIR &= ~(1 << PIN7);

        //init Magnetic Switch on PB3
        PORTB.PIN3CTRL |= PORT_OPC_PULLUP_gc;
        _delay_us(1);
        PORTB.DIR &= ~(1 << PIN3);

        //startup test
        PORTC.OUT |= (1 << PIN6);
        _delay_ms(250);
        PORTC.OUT &= ~(1 << PIN6);
        PORTC.OUT |= (1 << PIN7);
        _delay_ms(250);
        PORTC.OUT |= (1 << PIN6);
        PORTC.OUT &= ~(1 << PIN7);
        _delay_ms(250);
        PORTC.OUT &= ~(1 << PIN6);

}


void IR_LED (unsigned char state) {

        //--------------------------------------------------------------------------------
        //
        // Desc: changes the On/Off state of the IR leds
        //
        // Args:        state [0=OFF, 1=ON, 2=TOGGLE]
        //
        // Return:      none
        //
        //--------------------------------------------------------------------------------

        switch (state) {
    case (OFF):
        PORTA.OUT  &= ~(1 << PIN7); // turn IR LEDs off
        break;
    case (ON):
        PORTA.OUT |= (1 << PIN7); //turn IR LEDs on
        break;
    case (TOGGLE):
        PORTA.OUTTGL = (1 << PIN7); //toggle IR LED state
        break;
    default:
        break;
        }
}

void init_motors () {


        //--------------------------------------------------------------------------------
        //
        // Desc: Configures PWM for Motors 
        //
        // Args:        none
        //
        // Return:      none
        //
        //--------------------------------------------------------------------------------

       

        // Configure global MOTOR STBY pin on PD3
		 PORTD.PIN3CTRL |= PORT_OPC_PULLDOWN_gc; //pull down
        _delay_us(1); //let pullup rise
        PORTD.DIR |= (1 << PIN3); //set as output
        PORTD.OUT |= (1 << PIN3); //set high


        // Setup motor 1 outputs on PC2 and PC3
        PORTC.DIR |= (1 << PIN2);//set as output in direction register
        PORTC.DIR |= (1 << PIN3);
        
        //Setup Motor 2 outputs on PC4 and PC5
        PORTC.DIR |= (1 << PIN4);//set as output in direction register
        PORTC.DIR |= (1 << PIN5);

        //Set TC Periods
        TCC0.PER = 127;
        TCC1.PER = 127;
        
        //Set TCs to dual slope mode
        TCC0.CTRLB |= TC_WGMODE_DS_B_gc;
        TCC1.CTRLB |= TC_WGMODE_DS_B_gc;
        
        //Enable PWM Channels
        TCC0.CTRLB |= TC0_CCCEN_bm;
        TCC0.CTRLB |= TC0_CCDEN_bm;
        TCC1.CTRLB |= TC1_CCAEN_bm;
        TCC1.CTRLB |= TC1_CCBEN_bm;
        
        //Activate TCs by setting clock
        TCC0.CTRLA |= TC_CLKSEL_DIV8_gc;
        TCC1.CTRLA |= TC_CLKSEL_DIV8_gc;

        //Initialize Compare Registers to 0
        TCC0.CCCBUF = 0;
        TCC0.CCDBUF = 0;
        TCC1.CCABUF = 0;
        TCC1.CCBBUF = 0;
        
        //Set Motor 1 speed to 0
        MOTOR(1, 0);
        MOTOR(2, 0);
}

void MOTOR (unsigned char id, signed char speed) {

        //-----------------------------------------------------------------
        //
        // Desc:        Sets TCCA registers for motor speed
        //
        //      Arg:    id: Motor ID Number [1 or 2]
        //                      speed: desired motor speed percent [-100 to 100]
        //
        //      Return: none
        //
        //------------------------------------------------------------------

        double tempf;


        //clamp duty to valid range
        if (speed < 0)
                speed = 0;
        else if (speed > maxSpeed)
                speed = maxSpeed;

        //convert percent to 8-bit (-255 to 255) duty
        tempf = (double)speed * 255.0 / 100.0;

        switch (id) {
    case(1):
        //record current speed
        M1.speed = speed;

        //set Compare/Capture A register
        if (speed > 0) { //forward
            TCC0.CCCBUF = 0;
            TCC0.CCDBUF = (int)tempf;
        }
        else if (speed < 0) { //reverse
            TCC0.CCCBUF = (int)tempf*-1;
            TCC0.CCDBUF = 0;
        }
        else { //zero speed
            TCC0.CCCBUF = 0;
            TCC0.CCDBUF = 0;
        }
        break;
    case(2): 
        //record current speed
        M2.speed = speed;
                
        //set Compare/Capture A register
        if (speed > 0) { //forward
            TCC1.CCABUF = (int)tempf;
            TCC1.CCBBUF = 0;
        }
        else if (speed < 0) { //reverse
            TCC1.CCABUF =0;
            TCC1.CCBBUF = (int)tempf*-1;
        }
        else { //zero speed
            TCC1.CCABUF = 0;
            TCC1.CCBBUF = 0;
        }
        break;
    default:
        break;
        }

}
        
        
        

void LED (unsigned char id, unsigned char state) {

        
        //--------------------------------------------------------------------------------
        //
        // Desc:        changes the state of the user LEDs 
        //
        // Args:        none
        //
        // Return:      none
        //
        //--------------------------------------------------------------------------------


        unsigned char pin = 0;

        if (!((id == 1)||(id == 2)||(id == 3)||(id == 4)||(id == 5)))
                return;

		if ((id == 1)||(id == 2))
		{
        	pin = id + 5; //change id (1 or 2) to match port c bit (6 or 7)
			switch (state) {
    		case (OFF):
        		PORTC.OUT  &= ~(1 << pin); // turn LED off
        		break;
    		case (ON):
        		PORTC.OUT |= (1 << pin); //turn LED on
        		break;
    		case (TOGGLE):
        		PORTC.OUTTGL = (1 << pin); //toggle LED state
        		break;
    		default:
        		break;
			}
		}

		else if ((id == 3)||(id == 4)||(id == 5))
		{
			pin = id - 3; //change id (3, 4 or 5) to match port b bit (0, 1 or 2)
			switch (state) {
    		case (OFF):
        		PORTB.OUT  &= ~(1 << pin); // turn LED off
        		break;
    		case (ON):
        		PORTB.OUT |= (1 << pin); //turn LED on
        		break;
    		case (TOGGLE):
        		PORTB.OUTTGL = (1 << pin); //toggle LED state
        		break;
    		default:
        		break;
			}
        }

		else
			return;
}




#if DEBUG

void init_OLED() {

        //-------------------------------------------------------------------------------//
        //
        // Desc: initializes the OLED Screen
        //
        // Return: none
        //
        //-------------------------------------------------------------------------------//

        _delay_ms(1000);//startup delay for OLED screen

        newOLEDRX = 0;
        OLEDUSART_Trans('U');//auto baud-rate detect
        OLEDwait();

        newOLEDRX = 0;
        OLEDUSART_Trans('E');//clear screen
        OLEDwait();

        newOLEDRX = 0;
        OLEDUSART_Trans('O');//opaque/transparant text background
        OLEDUSART_Trans(0x01);//opaque text background
        OLEDwait();

        _delay_ms(200);//startup delay for OLED screen
}

void OLEDUSART_INIT(int BSEL) {

        //-------------------------------------------------------------------------------//
        //
        // Desc: Initializes the OLED USART with baud rate BSEL
        //
        // Return: none
        //

        OLEDUSARTPORT.OUT |= (1 << OLEDUSART_TX_PIN);//set TX pin high
        OLEDUSARTPORT.DIR |= (1 << OLEDUSART_TX_PIN);//set TX pin to output
        OLEDUSART.BAUDCTRLA = (BSEL & 0xFF);//set baud rate
        OLEDUSART.BAUDCTRLB = (BSEL >> 8) & 0x0F;//set baud rate
        OLEDUSART.CTRLC = USART_CHSIZE_8BIT_gc;//set 8N1 format
        OLEDUSART.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;//enable rx+tx

        OLEDUSART.CTRLA |= USART_RXCINTLVL_MED_gc;//set RX interrupt to medium priority
}


void OLEDUSART_Trans(unsigned char data) {

        //-------------------------------------------------------------------------------//
        //
        // Desc: sends control character 'data' to OLED
        //
        // Return: none
        //
        //-------------------------------------------------------------------------------//

        //      while(!(OLEDUSART.STATUS&USART_DREIF_bm));//wait for space in FIFO
        //      OLEDUSART.DATA=data;//load byte into FIFO
        if (buffLength - ((OLEDbuffIn - OLEDbuffOut) & buffMask)) {//drop packet if OLEDbuffer is full
                OLEDbuffer[OLEDbuffIn++] = data;
                OLEDbuffIn &= buffMask;
                OLEDUSART.CTRLA |= USART_DREINTLVL_MED_gc;//enable data register empty interrupt
        }
}


void OLEDwait() {

        //-------------------------------------------------------------------------------//
        //
        // Desc: waits up to OLED_TIMEOUT for a response from the OLED
        //
        // Return: none
        //
        //-------------------------------------------------------------------------------//

        unsigned int i;
        for (i = 0; i < OLED_TIMEOUT ; i++) {
                if (newOLEDRX) {
                        return;
                }
                _delay_ms(1);
        }
        badOLED = 1;
}


void OLED_string(unsigned char x, unsigned char y, char *str) {

        //-------------------------------------------------------------------------------//
        //
        // Desc: Prints a string to the OLED to row x, col y.
        //
        // Return: none
        //
        //-------------------------------------------------------------------------------//

        unsigned int i = 0;

        newOLEDRX = 0;
        OLEDUSART_Trans('s');
        OLEDUSART_Trans(x);
        OLEDUSART_Trans(y);
        OLEDUSART_Trans(0x02);
        OLEDUSART_Trans(0xFF);
        OLEDUSART_Trans(0xFF);

        do {
                OLEDUSART_Trans(str[i]);
        } while ((str[i++]) != 0);
        OLEDwait();
}


void OLED_int(long int n, unsigned char x, unsigned char y) {

        //-------------------------------------------------------------------------------//
        //
        // Desc: Prints a long int to the OLED to row x, col y.
        //
        // Return: none
        //
        //-------------------------------------------------------------------------------//

        char st[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        // The 0x30 addition shifts the decimal number up
        // to the ASCII location of "0".

        if (n < 0) {
                st[0] = '-';
                n = -n;
        } else {
                st[0] = '+';
        }

        st[1] = (n / 1000000000) + 0x30;
        n = n % 1000000000;

        st[2] = (n / 100000000) + 0x30;
        n = n % 100000000;

        st[3] = (n / 10000000) + 0x30;
        n = n % 10000000;

        st[4] = (n / 1000000) + 0x30;
        n = n % 1000000;

        st[5] = (n / 100000) + 0x30;
        n = n % 100000;

        st[6] = (n / 10000) + 0x30;
        n = n % 10000;

        st[7] = (n / 1000) + 0x30;
        n = n % 1000;

        st[8] = (n / 100) + 0x30;
        n = n % 100;

        st[9] = (n / 10) + 0x30;
        n = n % 10;

        st[10] = n + 0x30;

        // Print it as a string
        OLED_string(x, y, (char *)st);
}

#endif


