#ifndef _pot_h
#define _pot_h

void pot_init();
uint16_t pot_read(uint8_t i);

#endif